<?php
require_once("animal.php");
require_once("Frog.php");
require_once("ape.php");
$sheep = new Animal("shaun");

echo "Nama : $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 2
echo "Darah : $sheep->cold_blooded <br> <br>"; // false

$kodok = new Frog("buduk");
echo "Nama : $kodok->name <br>";
echo "Jumlah Kaki : $kodok->legs <br>";
echo "Darah : $kodok->cold_blooded <br>";
$kodok->jump(); // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Nama : $sungokong->name <br>";
echo "Jumlah Kaki : $sungokong->legs <br>";
echo "Darah : $sungokong->cold_blooded <br>";
$sungokong->yell(); // "Auooo"
